# Fixdate

Use exiftool to fix the date of a file: set the creation and modification date to the date of the photo or video.
A typical use case is when you download photos from Google Photos and the date of the files is the date of the download.

## Pre-requisites

* [exiftool](https://exiftool.org/)
* Run on MacOS only but the script can be easily adapted to run on Linux

## Usage

### CLI

```bash
fixdate.sh <file|directory>
```

The files will be changed in place.

### MacOS App

Copy `fixdate.app` to your Applications folder and run it from there.
Select a file or a directory. The files will be changed in place.
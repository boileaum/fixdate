#!/bin/bash

export PATH=$PATH:/usr/local/bin:/opt/homebrew/bin/

# function to update file timestamp
function update_timestamp() {
    # get the creation date/time from the EXIF data
    create_time=$(exiftool -CreateDate -d "%Y%m%d%H%M.%S" "$1" | awk '{print $4}')

    # check if the EXIF data contains a valid date/time
    if [ -z "$create_time" ]; then
        echo "$1: does not contain a valid date/time in the EXIF data"
    else
        # set the modification and change time of the file to the creation date/time
        touch -m -t "$create_time" "$1"
        touch -a -t "$create_time" "$1"
        echo "$1: OK"
    fi
}

# Notify using osascript
function notify() {
    n_process_files=$(grep ": OK$" $1 | wc -l)
    text="Fichiers traités avec succès: $n_process_files"
    unprocess_files=$(grep -v ": OK$" $1 |cut -d ':' -f1)
    if [ -n "$unprocess_files" ]; then
        text="$text\nFichier non traité(s) :\n$unprocess_files"
    fi
    /usr/bin/osascript -e "display dialog \"$text\" with title \"Résumé de fixdate\"" > /dev/null
}

# check if a file or directory was specified as an argument
if [ -z "$1" ]; then
    echo "Usage: $0 <file or directory>"
    exit 1
fi

# create a temporary file to store the standard output
tmpfile=$(mktemp /tmp/fixdate.XXXXXX)

# check if the argument is a file or a directory
if [ -f "$1" ]; then
    # update the timestamp of a single file
    update_timestamp "$1" > "$tmpfile"
    notify "$tmpfile"
elif [ -d "$1" ]; then
    # update the timestamp of all files in a directory tree
    find "$1" -type f \( -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.png" -o -iname "*.gif" -o -iname "*.bmp" -o -iname "*.tiff" -o -iname "*.mov" -o -iname "*.mp4" \) -print0 | while read -d $'\0' file
    do 
        update_timestamp "$file" >> "$tmpfile"
    done
    notify "$tmpfile"
else
    echo "Error: $1 is not a file or directory"
    exit 1
fi